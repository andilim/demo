﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace JayLab.DrawShape.Controllers
{
    [Route("api/[controller]")]
    public class LuisParserController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost("[action]")]
        public Shape ParseLuisResponse(string responseBody)
        {
            var response = JsonConvert.DeserializeObject<LUISResponse>(responseBody);
            if (isValidIntent(response))
            {
                var result = new Shape();
                result.name = response.entities.First(entity => entity.type == "Shape").entity;
                result.properties = new Dictionary<string, decimal>();
                var properties = response.entities.Where(entity => entity.type == "ShapeProperty").Select(ent => ent.entity).ToArray();
                var propertiesValue = response.entities.Where(entity => entity.type == "builtin.number").Select(ent => ent.entity).ToArray();
                for (int i = 0; i < properties.Length; i++)
                {
                    result.properties.Add(properties[i].ToLower(), decimal.Parse(propertiesValue[i]));
                }

                if (validateShapeName(result))
                {
                    if (hasValidProperties(result))
                    {
                        return result;
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException(string.Format("Shape {0} does not have valid properties", result.name));
                    }
                }
                else
                {
                    throw new ArgumentOutOfRangeException(string.Format("Shape {0} is not supported", result.name));
                }
            }
            throw new InvalidOperationException("Request does not have valid intent.");
        }

        private bool isValidIntent(LUISResponse response)
        {
            // get the "DefineShape" intent
            var defineShapeIntent = response.intents.FirstOrDefault(intent => intent.intent == "DefineShape");
            return (defineShapeIntent != null && defineShapeIntent.score > 0.8);
        }

        private bool hasValidProperties(Shape shape)
        {
            switch (shape.name.ToLower())
            {
                case "isosceles triangle":
                case "scalene triangle":
                case "parallelogram":
                    return (shape.properties.Keys.Any(propName => propName.ToLower() == "base") && shape.properties.Keys.Any(propName => propName.ToLower() == "height"));

                case "square":
                case "pentagon":
                case "hexagon":
                case "octagon":
                case "heptagon":
                case "equilateral triangle":
                    return shape.properties.Keys.Any(propName => propName.ToLower() == "length");
                case "oval":
                case "rectangle":
                    return (shape.properties.Keys.Any(propName => propName.ToLower() == "width") && shape.properties.Keys.Any(propName => propName.ToLower() == "height"));
                case "circle":
                    return (shape.properties.Keys.Any(propName => propName.ToLower() == "diameter") || shape.properties.Keys.Any(propName => propName.ToLower() == "radius"));
            }

            return false;
        }



        private bool validateShapeName(Shape shape)
        {
            var supportedShapes = new string[] { "Isosceles Triangle", "Square", "Scalene Triangle", "Parallelogram", "Equilateral Triangle", "Pentagon", "Rectangle", "Hexagon", "Heptagon", "Octagon", "Circle", "Oval" };

            return supportedShapes.Any(name => name.ToLower() == shape.name.ToLower());
        }
    }

    public class LUISResponse
    {
        public string query { get; set; }
        public lIntent[] intents { get; set; }
        public lEntity[] entities { get; set; }
    }

    public class lIntent
    {
        public string intent { get; set; }
        public float score { get; set; }
    }

    public class lEntity
    {
        public string entity { get; set; }
        public string type { get; set; }
        public int startIndex { get; set; }
        public int endIndex { get; set; }
        public float score { get; set; }
    }

    public class Shape
    {
        public string name { get; set; }
        public Dictionary<string, decimal> properties { get; set; }
    }
}