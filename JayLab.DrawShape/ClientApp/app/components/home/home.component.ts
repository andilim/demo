import { Component, Inject, ViewChild } from '@angular/core';
import { Http, RequestOptions, Headers, RequestOptionsArgs } from "@angular/http";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs';
import { of } from 'rxjs/observable/of';
import { KonvaComponent, KonvaModule } from 'ng2-konva';

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})

export class HomeComponent {
    command: string = "";
    shape: any = null;
    error: any;
    configStage = new BehaviorSubject({ width: 800, height: 600 });
    parseLuisUrl: string;
    shapeType = "";

    circleData: any = {
        x: 100,
        y: 100,
        radius: 80,
        fill: 'transparent',
        stroke: 'black',
        strokeWidth: 4
    };

    ovalData: any = {
        x: 100,
        y: 100,
        radius: {
            x: 100,
            y: 50
        },
        fill: 'transparent',
        stroke: 'black',
        strokeWidth: 4
    };

    poligonData: any = {
        x: 100,
        y: 100,
        sides: 5,
        radius: 70,
        stroke: 'black',
        strokeWidth: 4,
        fill: 'transparent',
    };
    lineData: any = {
        points: [],
        fill: 'transparent',
        stroke: 'black',
        strokeWidth: 4,
        closed: true

    };
    rectData: any = {
        x: 50,
        y: 50,
        width: 100,
        height: 50,
        fill: 'transparent',
        stroke: 'black',
        strokeWidth: 4
    };

    configShape: Observable<any> = of(this.circleData);
    
    constructor(private httpClient: Http, @Inject('BASE_URL') baseUrl: string) {
        this.parseLuisUrl = baseUrl + 'api/LuisParser/ParseLuisResponse';
    }

    draw() {
        var queryUrl = "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/be9ecb12-83a5-47ac-9e46-242872b6fac5?subscription-key=a02d408380d7447f8eb52d59d48ee644&staging=true&verbose=true&timezoneOffset=600&q="
            + this.command;

        this.httpClient.get(queryUrl)
            .subscribe((data: any) => {
                var body = data._body;
                this.parseLuisResult(body);
            }, error => this.error = error);;

    }

    private parseLuisResult(responseBody: string) {
        var data = { data: { responseBody: responseBody } };
        this.httpClient.post(this.parseLuisUrl, null, <RequestOptionsArgs>{
            params: {
                responseBody: responseBody
            }
        }).subscribe((result: any) => {
            this.shape = JSON.parse(result._body);
            this.shapeType = "";
            
            if (this.shape.name.toLowerCase() === 'circle') {
                this.createConfigCircle();
            } else if (this.shape.name.toLowerCase().indexOf('triangle') >= 0) {
                this.createConfigTriangle();
            } else if (this.shape.name.toLowerCase() === 'square' || this.shape.name.toLowerCase() === 'rectangle') {
                this.createConfigRectangle();
            } else if (this.shape.name.toLowerCase() === "parallelogram") {
                var base = this.shape.properties["base"];
                var height = this.shape.properties["height"];
                var leftMargin = 20;
                var topMargin = 25;
                var offset = base / 4;

                // clear the array
                this.lineData.points = [];

                // first point (leftMargin + offset, topMargin + )
                this.lineData.points.push(leftMargin + offset);
                this.lineData.points.push(topMargin);

                // second point (leftMargin + offset + base, topMargin);
                this.lineData.points.push(leftMargin + base + offset);
                this.lineData.points.push(topMargin);

                // third point (leftMargin + base , height + topMargin)
                this.lineData.points.push(leftMargin + base);
                this.lineData.points.push(topMargin + height);

                // fourth point (leftMargin, height + topMargin)
                this.lineData.points.push(leftMargin);
                this.lineData.points.push(topMargin + height);

                this.configStage.getValue().height = height + 200;
                this.configStage.getValue().width = base + 200;
                this.configShape = of(this.lineData);
                this.shapeType = "line";
            } else if (this.shape.name.toLowerCase() === 'pentagon') {
                this.createConfigPoligon(5);
            } else if (this.shape.name.toLowerCase() === 'hexagon') {
                this.createConfigPoligon(6);
            } else if (this.shape.name.toLowerCase() === 'heptagon') {
                this.createConfigPoligon(7);
            } else if (this.shape.name.toLowerCase() === 'octagon') {
                this.createConfigPoligon(8);
            } else if (this.shape.name.toLowerCase() === 'oval') {
                this.createConfigOval();
            }



        }, (error) => {
           this.error = error;
        });
    }

    private createConfigCircle() {
        var radius: number = 0;
        if (this.shape.properties["radius"]) {
            radius = this.shape.properties["radius"];
        } else {
            radius = this.shape.properties["diameter"] / 2;
        }
        this.configStage.getValue().height = radius + 200;
        this.configStage.getValue().width = radius + 200;
        this.circleData.x = radius + 20;
        this.circleData.y = radius + 20;
        this.circleData.radius = radius;
        this.configShape = of(this.circleData);
        this.shapeType = "circle";
    }

    private createConfigOval() {
        var width = 0;
        var height = 0;
        width = this.shape.properties["width"];
        height = this.shape.properties["height"];
        this.ovalData.x = 120 + (width / 2);
        this.ovalData.y = 120 + (height / 2);
        this.ovalData.radius.x = width;
        this.ovalData.radius.y= height;

        this.configStage.getValue().width = width + 200;
        this.configStage.getValue().height = height + 200;

        this.configShape = of(this.ovalData);
        this.shapeType = "oval";
    }

    private createConfigTriangle() {
        var leftMargin = 20;
        var topMargin = 25;

        if (this.shape.name.toLowerCase() === 'equilateral triangle') {
            this.createConfigPoligon(3);
        } else if (this.shape.name.toLowerCase() === 'isosceles triangle') {
            var base = this.shape.properties["base"];
            var height = this.shape.properties["height"];

            // clear the array
            this.lineData.points = [];
            // first point (leftMargin, height + topMargin)
            this.lineData.points.push(leftMargin);
            this.lineData.points.push(height + topMargin);

            // second point (leftMargin + base, height + topMargin);
            this.lineData.points.push(leftMargin + base);
            this.lineData.points.push(height + topMargin);

            // third point (leftMargin + (base / 2) + topMargin)
            this.lineData.points.push(leftMargin + (base / 2));
            this.lineData.points.push(topMargin);


            this.configStage.getValue().height = height + 200;
            this.configStage.getValue().width = base + 200;
            this.configShape = of(this.lineData);
            this.shapeType = "line";
        } else if (this.shape.name.toLowerCase() === 'scalene triangle') {
            var base = this.shape.properties["base"];
            var height = this.shape.properties["height"];

            // clear the array
            this.lineData.points = [];
            // first point (leftMargin, height + topMargin)
            this.lineData.points.push(leftMargin + (base == height ? 10 : 0));
            this.lineData.points.push(height + topMargin);

            // second point (leftMargin + base, height + topMargin);
            this.lineData.points.push(leftMargin + base);
            this.lineData.points.push(height + topMargin);

            // third point (leftMargin + (base / 2) + topMargin)
            this.lineData.points.push(leftMargin);
            this.lineData.points.push(topMargin);


            this.configStage.getValue().height = height + 200;
            this.configStage.getValue().width = base + 200;
            this.configShape = of(this.lineData);
            this.shapeType = "line";
        }
    }

    private createConfigPoligon(numberOfSides: number) {
        this.poligonData.sides = numberOfSides;
        this.poligonData.width = this.shape.properties["length"];
        this.poligonData.x = this.poligonData.width + 80
        this.poligonData.y = this.poligonData.width + 80

        this.configStage.getValue().height = this.poligonData.width + 200;
        this.configStage.getValue().width = this.poligonData.width + 200;
        this.configShape = of(this.poligonData);
        this.shapeType = "regular-polygon";
    }

    private createConfigRectangle() {
        var width = 0;
        var height = 0;
        if (this.shape.name.toLowerCase() === 'square') {
            width = this.shape.properties["length"];
            height = width;

        } else {
            width = this.shape.properties["width"];
            height = this.shape.properties["height"];
        }
        this.rectData.x = 20 + (width / 2);
        this.rectData.y = 20 + (height / 2);
        this.rectData.width = width;
        this.rectData.height = height;

        this.configStage.getValue().width = width + 200;
        this.configStage.getValue().height = height + 200;

        this.configShape = of(this.rectData);
        this.shapeType = "rectangle";
    }
}
